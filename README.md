## PHP Repo Parser for F-Droid
This repo contains a PHP class to parse and use the `index.xml`/`index-v1.json`/`index-v2.json`
files of an F-Droid repository. I'm using it regularly now for several years now with all
three formats, so it should be quite stable – though it's far from being perfect. Not much
documentation for now. Feel free to use it with your own projects as long as
the license fits.

One goal of this project is to provide a stable API: regardless of which index version
you are using, the structures provided via this API should be compatible (with attributes
only provided by newer index versions, say v2, just not available with older ones like v1
or v0 (XML)). As newer index versions sometimes also re-organize structures (like v2 did
with the localized attributes), this API moves back those attributes to where they had
been previously – meaning two things: 1) you can use each of the existing index versions
with the same library getting the same structures, and 2) you should be able to update to
a newer version of this API easily, taking it as drop-in-replacement without "breaking
stuff". If you still want the "newer structures": starting with implementing index-v2 a
constant will be checked. If you define `FREPP_KEEP_EXTENDED` and set it to `1` (currently
setting to `true` would also work, but I reserve the option to use it step-wise in some
future version, so better use integers), the newer structures will be kept in parallel.
Be aware that with large indexes, the resulting in-memory-object can become quite large,
too: with the F-Droid.org index as of June 2023, a `print_r()` of the applist results in
a file of almost 80 MB with, but just about 45 MB without the "extended structures".

To get started, you might wish to take a look at the
[`doc/example.php`](doc/example.php) file which gives a short walk-through.
All source code is also documented using the
[Javadoc](https://en.wikipedia.org/wiki/Javadoc) style
[PHPDoc](https://en.wikipedia.org/wiki/PHPDoc). So take a look at the latter
Wikipedia page for possibilities to generate an API reference, if you need it.

Oh: In case you wonder about the name. Originally I've named the project
"Prepaf" (Php REpo PArser for Fdroid). But as not even I myself could remember
that name, I decided for something simpler: Frepp, which means as much as
"Fdroid REpo Parser (library for) PHP". Finally adopted to the F-Droid
namespace, it was renamed once more, to `php-fdroid`.

### Notes
* This class was intended to be used with a *local* repository. I'm using it
  with my own (with a little more than 600 apps in), where speed and
  processing-time seem fine. With many simultaneous sessions on larger repositories
  it however becomes quite memory hungry (see „in-memory-object“ above). So with
  my own repo meanwhile being beyond 1k apps, and F-Droid's beyond 4k, I've
  moved to using this library to import the structures into a database and have
  the clients use that instead.
* While it will not work with a remote repository, it will work with just the
  `index.xml` (or `index-v1.json` / `index-v2.json`) downloaded – with a few
  limitations: as the `.apk` files are not there then, their timestamps cannot be
  evaluated for the last-built-time, and if the `categories.txt` won't be available,
  it cannot be evaluated either). A quick test with F-Droid's own `index.xml` and
  `index-v1.json` (3,000+ apps at that time) showed speed still seems reasonable.
* Special addendum: If e.g. with a major update of fdroidserver, its app cache
  might get rebuild, that means the date an app was added to the repo gets lost.
  To take caution for that, you can keep that in the `Maintainer Notes` block
  using a line in the format `AppAdded: YYYY-MM-DD`. The library checks that and
  takes it with preference.

### License
This project uses the GPLv2 license as stated in the [LICENSE](LICENSE) file.


### Contribute
You like this library and want to contribute?

* Pull/Merge Requests are welcome!
* Motivate me e.g. by sending me some mBTC to `1FsfvUGUpoPkLvJboKAnuBXHZ1zN3hbBL1` :)
